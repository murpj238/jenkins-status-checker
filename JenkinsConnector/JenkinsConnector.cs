﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using Outlook = Microsoft.Office.Interop.Outlook;
using Newtonsoft.Json.Linq;

namespace JenkinsConnector
{
    public static class JenkinsConnector
    {

        public static MonitoredBuild CheckBuildStatus(MonitoredBuild monitoredBuild)
        {
            try
            {
                    var lastCompletedBuild = getJobUrl(monitoredBuild.UrlToCheck, "lastCompletedBuild");
                    var lastBuild = getJobUrl(monitoredBuild.UrlToCheck, "lastBuild");
                    var lastSuccessfulBuild = getJobUrl(monitoredBuild.UrlToCheck, "lastSuccessfulBuild");
                    var inProgressCase = !lastCompletedBuild.Equals(lastBuild);
                    var failedCase = !lastCompletedBuild.Equals(lastSuccessfulBuild);
                    switch (monitoredBuild.BuildStatus)
                    {
                        case BuildStates.Failed when inProgressCase:
                        case BuildStates.Passed when inProgressCase:
                        case BuildStates.InProgress when inProgressCase:
                            return new MonitoredBuild(monitoredBuild.UrlToCheck, BuildStates.InProgress);

                        case BuildStates.InProgress when failedCase:
                        case BuildStates.Failed when failedCase:
                            return new MonitoredBuild(monitoredBuild.UrlToCheck, BuildStates.Failed);

                        default:
                            return new MonitoredBuild(monitoredBuild.UrlToCheck, BuildStates.Passed);
                    }
            }
            catch(Exception ex)
            {
                using (var eventLog = new EventLog("Application"))
                {
                    eventLog.Source = "JenkinsCheckerService";
                    eventLog.WriteEntry(ex.Message,EventLogEntryType.Error);
                }
            }
            return null;
        }

        public static void EmailUser(string subject,string body, string fileLocation)
        {
            try
            {
                var app = new Outlook.Application();
                var message = (Outlook.MailItem) app.CreateItem(Outlook.OlItemType.olMailItem);
                message.Body = body;
                message.Subject = subject;
                var recipient = message.Recipients;
                foreach (var email in GetFileEntries(fileLocation))
                {
                    if (!string.IsNullOrEmpty(email)) recipient.Add(email.Trim());
                }
                recipient.ResolveAll();
                message.Send();

                recipient = null;
                message = null;
                app = null;

            }
            catch (Exception ex)
            {
                using (var eventLog = new EventLog("Application"))
                {
                    eventLog.Source = "JenkinsCheckerService";
                    eventLog.WriteEntry(ex.Message,EventLogEntryType.Error);
                }
            }
        }

        public static IEnumerable<string> GetFileEntries(string fileName)
        {
            if (File.Exists(fileName))
                return File.ReadAllLines(fileName).ToList();

            using (var eventLog = new EventLog("Application"))
            {
                eventLog.Source = "JenkinsCheckerService";
                eventLog.WriteEntry($"{fileName} does not exist",EventLogEntryType.Error);
            }
            return new List<string> { "jack.murphy@test.ie"};
        }

        private static string getJobUrl(string baseUrl,string jobToGet)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Add("User-Agent",
                    "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
                var response = httpClient
                    .GetStringAsync(new Uri(
                        $"{baseUrl}/{jobToGet}/api/json"))
                    .Result;
                var rel = JObject.Parse(response);
                return rel.GetValue("url").ToString();
            }
        }
    }
}
