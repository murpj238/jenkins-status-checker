﻿using System;
using System.Collections.Generic;

namespace JenkinsConnector
{
    public class MonitoredBuild
    {
        public string UrlToCheck { get; set; }

        public BuildStates BuildStatus { get; set; }

        public MonitoredBuild(string urlToCheck, BuildStates buildStatus)
        {
            UrlToCheck = urlToCheck;
            BuildStatus = buildStatus;
        }
    }

    public enum BuildStates
    {
        Passed,
        Failed,
        InProgress
    }
}
