﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Timers;
using JenkinsConnector;

namespace TestJenkinsService
{
    public class TestJenkinsService
    {
        private static List<MonitoredBuild> _monitoredBuilds = new List<MonitoredBuild>();
        private static readonly string FileLocation = ConfigurationManager.AppSettings["FileLocation"];

        static void Main()
        {
            foreach (var job in JenkinsConnector.JenkinsConnector.GetFileEntries(FileLocation + "\\JobList.txt"))
            {
                _monitoredBuilds.Add(new MonitoredBuild(job,BuildStates.Passed));
            }
            while (true)
            {            
                var newEntries = JenkinsConnector.JenkinsConnector.GetFileEntries(FileLocation + "\\JobList.txt")
                    .Except(_monitoredBuilds.Select(x => x.UrlToCheck));
                foreach (var newEntry in newEntries.ToList())
                {
                    _monitoredBuilds.Add(new MonitoredBuild(newEntry,BuildStates.Passed));
                }
                checkBuildStatus("RecipientsList.txt");
            }
        }

        private static void checkBuildStatus(string emailRecipientsFile)
        {
            var temporaryList = _monitoredBuilds;
            foreach (var monitoredBuild in _monitoredBuilds)
            {
                var result = JenkinsConnector.JenkinsConnector.CheckBuildStatus(monitoredBuild);
                if (result.BuildStatus == monitoredBuild.BuildStatus) continue;
                temporaryList.Remove(monitoredBuild);
                temporaryList.Add(result);
                switch (result.BuildStatus)
                {
                    case BuildStates.Failed:
                        JenkinsConnector.JenkinsConnector.EmailUser("Build Failed or Aborted",
                            $"The last build has failed, please refer to the job here: {result.UrlToCheck}",
                            $"{FileLocation}\\{emailRecipientsFile}");
                        break;
                    case BuildStates.InProgress:
                        JenkinsConnector.JenkinsConnector.EmailUser("Build In Progress",
                            $"A build is now in progress for {result.UrlToCheck}",
                            $"{FileLocation}\\{emailRecipientsFile}");
                        break;
                    case BuildStates.Passed:
                        JenkinsConnector.JenkinsConnector.EmailUser("Build Passing :)",
                            $"The last build for {result.UrlToCheck} passed",
                            $"{FileLocation}\\{emailRecipientsFile}");
                        break;
                    default:
                        break;
                }
            }
            _monitoredBuilds = temporaryList;
        }
    }
}
