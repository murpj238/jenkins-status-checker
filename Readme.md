This project is a basic Windows Service that takes in a list of Jenkins jobs and reports on the Status of the job.

If the status of the job changes it sends an email from the local Outlook client to a specified list of recipients.

Ensure that you open the App.config and add a value to the FileLocation tag. This location should contain two files JobList.txt and Recipients.txt. These files will be used by the service.

To install the service compile the solution and run the following in a PowerShell session:

```powershell
InstallUtil.exe /user:<Username> /pass:<Password> <Path to Executable>
```

