﻿using System.ServiceProcess;

namespace JenkinsChecker
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            var servicesToRun = new ServiceBase[]
            {
                new JenkinsCheckerService()
            };
            ServiceBase.Run(servicesToRun);
        }
    }
}
