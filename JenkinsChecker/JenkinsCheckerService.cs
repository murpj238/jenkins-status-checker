﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceProcess;
using System.Timers;
using JenkinsConnector;

namespace JenkinsChecker
{
    public partial class JenkinsCheckerService : ServiceBase
    {
        private List<MonitoredBuild> _monitoredBuilds = new List<MonitoredBuild>();
        private Timer _timer;
        private readonly string _fileLocation = ConfigurationManager.AppSettings["FileLocation"];


        public JenkinsCheckerService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            foreach (var job in JenkinsConnector.JenkinsConnector.GetFileEntries(_fileLocation + "\\JobList.txt"))
            {
                _monitoredBuilds.Add(new MonitoredBuild(job,BuildStates.Passed));
            }
            _timer = new Timer(60 * 1000); // every 60 seconds
            _timer.Elapsed += timer_Elapsed;
            _timer.Start();
        }
        protected override void OnStop()
        {
        }
    
        private void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            var newEntries = JenkinsConnector.JenkinsConnector.GetFileEntries(_fileLocation + "\\JobList.txt")
                .Except(_monitoredBuilds.Select(x => x.UrlToCheck));
            foreach (var newEntry in newEntries.ToList())
            {
                _monitoredBuilds.Add(new MonitoredBuild(newEntry,BuildStates.Passed));
            }
            CheckBuildStatus("RecipientsList.txt");
        }

        private void CheckBuildStatus(string emailRecipientsFile)
        {
            foreach (var monitoredBuild in _monitoredBuilds)
            {
                var result = JenkinsConnector.JenkinsConnector.CheckBuildStatus(monitoredBuild);
                if (result.BuildStatus == monitoredBuild.BuildStatus) continue;
                switch (result.BuildStatus)
                {
                    case BuildStates.Failed:
                        JenkinsConnector.JenkinsConnector.EmailUser("Build Failed or Aborted",
                            $"The last build has failed, please refer to the job here: {result.UrlToCheck}",
                            $"{_fileLocation}\\{emailRecipientsFile}");
                        break;
                    case BuildStates.InProgress:
                        JenkinsConnector.JenkinsConnector.EmailUser("Build In Progress",
                            $"A build is now in progress for {result.UrlToCheck}",
                            $"{_fileLocation}\\{emailRecipientsFile}");
                        break;
                    case BuildStates.Passed:
                        JenkinsConnector.JenkinsConnector.EmailUser("Build Passing :)",
                            $"The last build for {result.UrlToCheck} passed",
                            $"{_fileLocation}\\{emailRecipientsFile}");
                        break;
                }
                monitoredBuild.BuildStatus = result.BuildStatus;
            }
        }
    }
}
